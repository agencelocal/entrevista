jQuery(document).ready(function ($) {
    $('#data-inicio, #data-fim').mask('00/00/0000');

    $('#clientes').select2({
        placeholder: 'Selecione os Clientes',
        language: 'pt-BR',
    });

    $('.datepicker').datepicker({
        todayBtn: "linked",
        clearBtn: true,
        language: "pt-BR",
        autoclose: true,
        todayHighlight: true
    });
});

function ajaxReport(tipoRelatorio) {

    $('.btn-report').removeClass('active');
    if ($('#clientes').val()[0] === '' || $('#clientes').val()[0] === undefined) {
        alert('Selecione pelo menos 1 cliente!');
        return;
    }
    if ($('#data-inicio').val() === '') {
        alert('Digite a data de início do período desejado!');
        $('#data-inicio').focus();
        return;
    }
    if ($('#data-fim').val() === '') {
        alert('Digite a data fim do período desejado!');
        $('#data-fim').focus();
        return;
    }

    $('#content-report').children().remove();
    $('#title-report').hide();
    $('#loading-report').show();

    switch (tipoRelatorio) {
        case 'relatório':
            $('#btn-report').addClass('active');
            $('#description-main-report').text('Receita Detalhada');
            $('#description-report').text('Receita líquida de cada cliente');
            $.ajax({
                method: "POST",
                url: '/cao-cliente/report',
                dataType: 'json',
                data: $('#form-filter').serialize(),
                headers: {
                    'X-CSRF-Token': $('#csrfToken').val()
                },
                beforeSend: function () {
                }
            }).done(function (json) {
                $('#content-report').append(json.table);
                $('#title-report').show();
                $('#loading-report').hide();
            }).always(function (jqXHR, textStatus, errorThrown) {
                //console.log('jqXHR 1: ', jqXHR);
                //console.log('textStatus 1: ', textStatus);
                //console.log('errorThrown 1: ', errorThrown);
                //$('body').append(jqXHR.responseText);
            });
            break;
        case 'gráfico':
            $('#btn-graph').addClass('active');
            $('#description-main-report').text('Desempenho em Receita');
            $('#description-report').text('Desempenho em receita líquida de cada cliente');
            $.ajax({
                method: "POST",
                url: '/cao-cliente/graph',
                dataType: 'json',
                data: $('#form-filter').serialize(),
                headers: {
                    'X-CSRF-Token': $('#csrfToken').val()
                },
                beforeSend: function () {
                }
            }).done(function (json) {
                $('#content-report').append('<canvas id="chartjs-graph" class="chartjs" style="width: 90%; max-width: 90%; height: 300px; max-height: 300px;"></canvas>');

                new Chart(document.getElementById("chartjs-graph"),
                        {
                            "type": "line",
                            "data": {
                                "labels": json.labels,
                                "datasets": json.datasets
                            },
                            "options": {
                                responsive: true,
                                scales: {
                                    yAxes: [{
                                            ticks: {
                                                callback: function (value) {
                                                    var money = 'R$ ' + float2money(value);
                                                    return money;
                                                }
                                            }
                                        }]
                                },
                                tooltips: {
                                    mode: 'label',
                                    label: 'mylabel',
                                    callbacks: {
                                        label: function (tooltipItem, data) {
                                            var money = 'R$ ' + float2money(tooltipItem.yLabel);
                                            return money;
                                        },
                                    },
                                },
                            }
                        }
                );

                $('#title-report').show();
                $('#loading-report').hide();
            }).always(function (jqXHR, textStatus, errorThrown) {
                //console.log('jqXHR 1: ', jqXHR);
                //console.log('textStatus 1: ', textStatus);
                //console.log('errorThrown 1: ', errorThrown);
                //$('body').append(jqXHR.responseText);
            });
            break;
        case 'pizza':
            $('#btn-pizza').addClass('active');
            $('#description-main-report').text('Percentual de Receita');
            $('#description-report').text('Percentual de receita líquida gerada por cada cliente');
            $.ajax({
                method: "POST",
                url: '/cao-cliente/pizza',
                dataType: 'json',
                data: $('#form-filter').serialize(),
                headers: {
                    'X-CSRF-Token': $('#csrfToken').val()
                },
                beforeSend: function () {
                }
            }).done(function (json) {
                $('#content-report').append('<canvas id="chartjs-pie" class="chartjs" style="width: 90%; max-width: 90%; height: 300px; max-height: 300px;"></canvas>');
                new Chart(document.getElementById("chartjs-pie"),
                        {
                            "type": "pie",
                            "data": {
                                "labels": json.labels,
                                "datasets": json.datasets
                            },
                            "options": {
                                responsive: true,
                                tooltips: {
                                    mode: 'label',
                                    label: 'mylabel',
                                    callbacks: {
                                        label: function (tooltipItem, data) {
                                            var money = 'R$ ' + float2money(data.datasets[0].data[tooltipItem.index]);
                                            return money;
                                        },
                                    },
                                },
                            }
                        }
                );
                $('#title-report').show();
                $('#loading-report').hide();
            }).always(function (jqXHR, textStatus, errorThrown) {
                //console.log('jqXHR 1: ', jqXHR);
                //console.log('textStatus 1: ', textStatus);
                //console.log('errorThrown 1: ', errorThrown);
                //$('body').append(jqXHR.responseText);
            });
            break;
    }
}

/** Função para transformar float em formato de dinheiro. */
function float2money(num) {

    var x = 0, cents, ret;

    if (num < 0) {
        num = Math.abs(num);
        x = 1;
    }
    if (isNaN(num))
        num = "0";
    cents = Math.floor((num * 100 + 0.5) % 100);

    num = Math.floor((num * 100 + 0.5) / 100).toString();

    if (cents < 10)
        cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + '.'
                + num.substring(num.length - (4 * i + 3));
    ret = num + ',' + cents;
    if (x == 1)
        ret = ' - ' + ret;
    return ret;
}