<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PermissaoSistemaFixture
 *
 */
class PermissaoSistemaFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'permissao_sistema';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'co_usuario' => ['type' => 'string', 'length' => 20, 'null' => false, 'default' => '', 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'co_tipo_usuario' => ['type' => 'biginteger', 'length' => 20, 'unsigned' => true, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'co_sistema' => ['type' => 'biginteger', 'length' => 20, 'unsigned' => true, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'in_ativo' => ['type' => 'string', 'fixed' => true, 'length' => 1, 'null' => false, 'default' => 'S', 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'co_usuario_atualizacao' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'dt_atualizacao' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => '0000-00-00 00:00:00', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'co_usuario' => ['type' => 'index', 'columns' => ['co_usuario', 'co_tipo_usuario', 'co_sistema', 'dt_atualizacao'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['co_usuario', 'co_tipo_usuario', 'co_sistema'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'MyISAM',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'co_usuario' => 'a1859e0b-6815-4146-99de-ac56adddb651',
                'co_tipo_usuario' => 1,
                'co_sistema' => 1,
                'in_ativo' => 'L',
                'co_usuario_atualizacao' => 'Lorem ipsum dolor ',
                'dt_atualizacao' => '2018-11-26 21:42:29'
            ],
        ];
        parent::init();
    }
}
