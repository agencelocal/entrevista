<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CaoSalarioFixture
 *
 */
class CaoSalarioFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'cao_salario';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'co_usuario' => ['type' => 'string', 'length' => 20, 'null' => false, 'default' => '', 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'dt_alteracao' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => '0000-00-00', 'comment' => '', 'precision' => null],
        'brut_salario' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => ''],
        'liq_salario' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => ''],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['co_usuario', 'dt_alteracao'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'MyISAM',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'co_usuario' => '1edc2b29-ad57-45c3-b558-d9759a97b553',
                'dt_alteracao' => '2018-11-27',
                'brut_salario' => 1,
                'liq_salario' => 1
            ],
        ];
        parent::init();
    }
}
