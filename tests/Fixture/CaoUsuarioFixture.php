<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CaoUsuarioFixture
 *
 */
class CaoUsuarioFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'cao_usuario';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'co_usuario' => ['type' => 'string', 'length' => 20, 'null' => false, 'default' => '', 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'no_usuario' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'ds_senha' => ['type' => 'string', 'length' => 14, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'co_usuario_autorizacao' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'nu_matricula' => ['type' => 'biginteger', 'length' => 22, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'dt_nascimento' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => '0000-00-00', 'comment' => '', 'precision' => null],
        'dt_admissao_empresa' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => '0000-00-00', 'comment' => '', 'precision' => null],
        'dt_desligamento' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'dt_inclusao' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => '0000-00-00 00:00:00', 'comment' => '', 'precision' => null],
        'dt_expiracao' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => '0000-00-00', 'comment' => '', 'precision' => null],
        'nu_cpf' => ['type' => 'string', 'length' => 14, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'nu_rg' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'no_orgao_emissor' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'uf_orgao_emissor' => ['type' => 'string', 'length' => 2, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'ds_endereco' => ['type' => 'string', 'length' => 150, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'no_email' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'no_email_pessoal' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'nu_telefone' => ['type' => 'string', 'length' => 64, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'dt_alteracao' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => '0000-00-00 00:00:00', 'comment' => '', 'precision' => null],
        'url_foto' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'instant_messenger' => ['type' => 'string', 'length' => 80, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'icq' => ['type' => 'integer', 'length' => 13, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'msn' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'yms' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'ds_comp_end' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'ds_bairro' => ['type' => 'string', 'length' => 30, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'nu_cep' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'no_cidade' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'uf_cidade' => ['type' => 'string', 'length' => 2, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'dt_expedicao' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'co_usuario_2' => ['type' => 'index', 'columns' => ['co_usuario', 'no_usuario', 'dt_alteracao'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['co_usuario'], 'length' => []],
            'co_usuario' => ['type' => 'unique', 'columns' => ['co_usuario'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'co_usuario' => 'e5e68177-20e1-413b-a869-3bbb3bc3d059',
                'no_usuario' => 'Lorem ipsum dolor sit amet',
                'ds_senha' => 'Lorem ipsum ',
                'co_usuario_autorizacao' => 'Lorem ipsum dolor ',
                'nu_matricula' => 1,
                'dt_nascimento' => '2018-11-26',
                'dt_admissao_empresa' => '2018-11-26',
                'dt_desligamento' => '2018-11-26',
                'dt_inclusao' => '2018-11-26 21:43:35',
                'dt_expiracao' => '2018-11-26',
                'nu_cpf' => 'Lorem ipsum ',
                'nu_rg' => 'Lorem ipsum dolor ',
                'no_orgao_emissor' => 'Lorem ip',
                'uf_orgao_emissor' => 'Lo',
                'ds_endereco' => 'Lorem ipsum dolor sit amet',
                'no_email' => 'Lorem ipsum dolor sit amet',
                'no_email_pessoal' => 'Lorem ipsum dolor sit amet',
                'nu_telefone' => 'Lorem ipsum dolor sit amet',
                'dt_alteracao' => '2018-11-26 21:43:35',
                'url_foto' => 'Lorem ipsum dolor sit amet',
                'instant_messenger' => 'Lorem ipsum dolor sit amet',
                'icq' => 1,
                'msn' => 'Lorem ipsum dolor sit amet',
                'yms' => 'Lorem ipsum dolor sit amet',
                'ds_comp_end' => 'Lorem ipsum dolor sit amet',
                'ds_bairro' => 'Lorem ipsum dolor sit amet',
                'nu_cep' => 'Lorem ip',
                'no_cidade' => 'Lorem ipsum dolor sit amet',
                'uf_cidade' => 'Lo',
                'dt_expedicao' => '2018-11-26'
            ],
        ];
        parent::init();
    }
}
