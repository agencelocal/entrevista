<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CaoOsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CaoOsTable Test Case
 */
class CaoOsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CaoOsTable
     */
    public $CaoOs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cao_os'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CaoOs') ? [] : ['className' => CaoOsTable::class];
        $this->CaoOs = TableRegistry::getTableLocator()->get('CaoOs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CaoOs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
