<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CaoClienteTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CaoClienteTable Test Case
 */
class CaoClienteTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CaoClienteTable
     */
    public $CaoCliente;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cao_cliente'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CaoCliente') ? [] : ['className' => CaoClienteTable::class];
        $this->CaoCliente = TableRegistry::getTableLocator()->get('CaoCliente', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CaoCliente);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
