<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PermissaoSistemaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PermissaoSistemaTable Test Case
 */
class PermissaoSistemaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PermissaoSistemaTable
     */
    public $PermissaoSistema;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.permissao_sistema'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PermissaoSistema') ? [] : ['className' => PermissaoSistemaTable::class];
        $this->PermissaoSistema = TableRegistry::getTableLocator()->get('PermissaoSistema', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PermissaoSistema);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
