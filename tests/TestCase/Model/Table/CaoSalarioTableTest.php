<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CaoSalarioTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CaoSalarioTable Test Case
 */
class CaoSalarioTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CaoSalarioTable
     */
    public $CaoSalario;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cao_salario'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CaoSalario') ? [] : ['className' => CaoSalarioTable::class];
        $this->CaoSalario = TableRegistry::getTableLocator()->get('CaoSalario', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CaoSalario);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
