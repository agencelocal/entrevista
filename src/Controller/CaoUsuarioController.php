<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Http\Response;

/**
 * CaoUsuario Controller
 *
 * @property \App\Model\Table\CaoUsuarioTable $CaoUsuario
 *
 * @method \App\Model\Entity\CaoUsuario[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CaoUsuarioController extends AppController {

    public function reports() {

        $consultores = $this->CaoUsuario->find('list', ['limit' => 200])
                ->where([
                    'PermissaoSistema.co_sistema' => 1,
                    'PermissaoSistema.in_ativo' => 's',
                    'PermissaoSistema.co_tipo_usuario IN' => [0, 1, 2]
                ])
                ->order(['CaoUsuario.no_usuario' => 'ASC'])
                ->join([
            'PermissaoSistema' => [
                'table' => 'permissao_sistema',
                'type' => 'INNER',
                'conditions' => 'PermissaoSistema.co_usuario = CaoUsuario.co_usuario'
            ],
        ]);
        $this->set(compact('consultores'));
    }

    public function report() {
        if ($this->request->is('post')) {

            $dateBeginGet = $this->request->getData('data_inicio');
            $dateEndGet = $this->request->getData('data_fim');

            /** Array com nome dos meses do ano, para formatar array. */
            $arrayMonth = [
                1 => 'Jan',
                2 => 'Fev',
                3 => 'Mar',
                4 => 'Abr',
                5 => 'Mai',
                6 => 'Jun',
                7 => 'Jul',
                8 => 'Ago',
                9 => 'Set',
                10 => 'Out',
                11 => 'Nov',
                12 => 'Dez'
            ];

            $auxMonthBegin = substr($dateBeginGet, 3, 2);
            $auxMonthEnd = substr($dateEndGet, 3, 2);

            $keys = $this->request->getData('consultores');
            $arrayDefault = array_fill_keys($keys, ['receita_liquida' => 0, 'custo_fixo' => 0, 'comissao' => 0, 'lucro' => 0]);

            $query = $this->CaoUsuario
                    ->find('list', [
                        'keyField' => 'co_usuario',
                        'valueField' => 'no_usuario'
                    ])
                    ->where([
                        'CaoUsuario.co_usuario IN' => $this->request->getData('consultores'),
                    ])
                    ->contain([])
                    ->order(['CaoUsuario.co_usuario' => 'ASC']);
            $consultores = $query->toArray();

            if (($auxMonthBegin != $auxMonthEnd) || (substr($dateBeginGet, 6, 4) != substr($dateEndGet, 6, 4))) {

                /** Se for de anos diferentes, então, percorre 2 for's: Primeiro até o fim do ano da primeira data e outro do mês 01
                 * até o mês do ano da segunda data. */
                if (substr($dateBeginGet, 6, 4) != substr($dateEndGet, 6, 4)) {

                    /** Percorre os meses começando no mês da primeira data até Dezembro. */
                    for ($i = (int) $auxMonthBegin; $i <= 12; $i++) {

                        if ($i == $auxMonthBegin) {
                            $day = substr($dateBeginGet, 0, 2);
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateBeginGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = $year . '-' . $month . '-' . substr($dateBeginGet, 0, 2);
                        } else {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateBeginGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year));
                        }
                        $receitas[] = $this->__querySumPerConsultantReport($this->request->getData('consultores'), $dataInicio, $dataFim, $arrayDefault);
                        $periodos[] = $arrayMonth[$i] . '/' . $year;
                    }

                    /** Percorre os anos intermediários das duas datas de Janeiro atẽ Dezembro. */
                    $nextYear = ((int) substr($dateBeginGet, 6, 4)) + 1;
                    $lastYear = ((int) substr($dateEndGet, 6, 4)) - 1;
                    for ($j = $nextYear; $j <= $lastYear; $j++) {
                        for ($i = 1; $i <= 12; $i++) {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = $j;
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year));

                            $receitas[] = $this->__querySumPerConsultantReport($this->request->getData('consultores'), $dataInicio, $dataFim, $arrayDefault);
                            $periodos[] = $arrayMonth[$i] . '/' . $year;
                        }
                    }

                    /** Percorre começando de Janeiro até o mês final correspondente a segunda data. */
                    for ($i = 1; $i <= (int) $auxMonthEnd; $i++) {

                        if ($i == $auxMonthEnd) {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateEndGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = $year . '-' . $month . '-' . substr($dateEndGet, 0, 2);
                        } else {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateEndGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year));
                        }
                        $receitas[] = $this->__querySumPerConsultantReport($this->request->getData('consultores'), $dataInicio, $dataFim, $arrayDefault);
                        $periodos[] = $arrayMonth[$i] . '/' . $year;
                    }
                } else {
                    /** Se for de anos iguais, então, percorre contando do mês referente a primeira data até o mês referente a
                     * segunda data. */
                    for ($i = (int) $auxMonthBegin; $i <= (int) $auxMonthEnd; $i++) {

                        if ($i == (int) $auxMonthBegin) {
                            $day = substr($dateBeginGet, 0, 2);
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateBeginGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day . ' 00:00';
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year)) . ' 23:59';
                        } else if ($i == (int) $auxMonthEnd) {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateEndGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day . ' 00:00';
                            $dataFim = $year . '-' . $month . '-' . substr($dateEndGet, 0, 2) . ' 23:59';
                        } else {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateBeginGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day . ' 00:00';
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year)) . ' 23:59';
                        }
                        $receitas[] = $this->__querySumPerConsultantReport($this->request->getData('consultores'), $dataInicio, $dataFim, $arrayDefault);
                        $periodos[] = $arrayMonth[$i] . '/' . $year;
                    }
                }
            } else {
                /** Se for do mesmo mês e ano. */
                for ($i = (int) substr($dateBeginGet, 0, 2); $i <= (int) substr($dateEndGet, 0, 2); $i++) {

                    $day = ($i < 10) ? '0' . $i : $i;
                    $dataInicio = substr($dateBeginGet, 6, 4) . '-' . substr($dateBeginGet, 3, 2) . '-' . $day;
                    $dataFim = substr($dateBeginGet, 6, 4) . '-' . substr($dateBeginGet, 3, 2) . '-' . $day;

                    $receitas[] = $this->__querySumPerConsultantReport($this->request->getData('consultores'), $dataInicio, $dataFim, $arrayDefault);
                    $periodos[] = $day . '/' . $arrayMonth[(int) substr($dateBeginGet, 3, 2)];
                }
            }
            $this->set(compact('receitas', 'consultores', 'periodos'));
        }
    }

    public function graph() {
        if ($this->request->is('post')) {

            $dateBeginGet = $this->request->getData('data_inicio');
            $dateEndGet = $this->request->getData('data_fim');

            /** Array com nome dos meses do ano, para formatar array. */
            $arrayMonth = [
                1 => 'Jan',
                2 => 'Fev',
                3 => 'Mar',
                4 => 'Abr',
                5 => 'Mai',
                6 => 'Jun',
                7 => 'Jul',
                8 => 'Ago',
                9 => 'Set',
                10 => 'Out',
                11 => 'Nov',
                12 => 'Dez'
            ];

            $auxMonthBegin = substr($dateBeginGet, 3, 2);
            $auxMonthEnd = substr($dateEndGet, 3, 2);

            $keys = $this->request->getData('consultores');
            $arrayDefault = array_fill_keys($keys, ['receita_liquida' => 0, 'custo_fixo' => 0]);

            $query = $this->CaoUsuario
                    ->find('list', [
                        'keyField' => 'co_usuario',
                        'valueField' => 'no_usuario'
                    ])
                    ->where([
                        'CaoUsuario.co_usuario IN' => $this->request->getData('consultores'),
                    ])
                    ->contain([])
                    ->order(['CaoUsuario.co_usuario' => 'ASC']);
            $consultores = $query->toArray();

            if (($auxMonthBegin != $auxMonthEnd) || (substr($dateBeginGet, 6, 4) != substr($dateEndGet, 6, 4))) {

                /** Se for de anos diferentes, então, percorre 2 for's: Primeiro até o fim do ano da primeira data e outro do mês 01
                 * até o mês do ano da segunda data. */
                if (substr($dateBeginGet, 6, 4) != substr($dateEndGet, 6, 4)) {

                    /** Percorre os meses começando no mês da primeira data até Dezembro. */
                    for ($i = (int) $auxMonthBegin; $i <= 12; $i++) {

                        if ($i == $auxMonthBegin) {
                            $day = substr($dateBeginGet, 0, 2);
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateBeginGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = $year . '-' . $month . '-' . substr($dateBeginGet, 0, 2);
                        } else {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateBeginGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year));
                        }
                        $receitas[] = $this->__querySumPerConsultantGraph($this->request->getData('consultores'), $dataInicio, $dataFim, $arrayDefault);
                        $periodos[] = $arrayMonth[$i] . '/' . $year;
                    }

                    /** Percorre os anos intermediários das duas datas de Janeiro atẽ Dezembro. */
                    $nextYear = ((int) substr($dateBeginGet, 6, 4)) + 1;
                    $lastYear = ((int) substr($dateEndGet, 6, 4)) - 1;
                    for ($j = $nextYear; $j <= $lastYear; $j++) {
                        for ($i = 1; $i <= 12; $i++) {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = $j;
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year));

                            $receitas[] = $this->__querySumPerConsultantGraph($this->request->getData('consultores'), $dataInicio, $dataFim, $arrayDefault);
                            $periodos[] = $arrayMonth[$i] . '/' . $year;
                        }
                    }

                    /** Percorre começando de Janeiro até o mês final correspondente a segunda data. */
                    for ($i = 1; $i <= (int) $auxMonthEnd; $i++) {

                        if ($i == $auxMonthEnd) {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateEndGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = $year . '-' . $month . '-' . substr($dateEndGet, 0, 2);
                        } else {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateEndGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year));
                        }
                        $receitas[] = $this->__querySumPerConsultantGraph($this->request->getData('consultores'), $dataInicio, $dataFim, $arrayDefault);
                        $periodos[] = $arrayMonth[$i] . '/' . $year;
                    }
                } else {
                    /** Se for de anos iguais, então, percorre contando do mês referente a primeira data até o mês referente a
                     * segunda data. */
                    for ($i = (int) $auxMonthBegin; $i <= (int) $auxMonthEnd; $i++) {

                        if ($i == (int) $auxMonthBegin) {
                            $day = substr($dateBeginGet, 0, 2);
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateBeginGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day . ' 00:00';
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year)) . ' 23:59';
                        } else if ($i == (int) $auxMonthEnd) {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateEndGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day . ' 00:00';
                            $dataFim = $year . '-' . $month . '-' . substr($dateEndGet, 0, 2) . ' 23:59';
                        } else {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateBeginGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day . ' 00:00';
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year)) . ' 23:59';
                        }
                        $receitas[] = $this->__querySumPerConsultantGraph($this->request->getData('consultores'), $dataInicio, $dataFim, $arrayDefault);
                        $periodos[] = $arrayMonth[$i] . '/' . $year;
                    }
                }
            } else {
                /** Se for do mesmo mês e ano. */
                for ($i = (int) substr($dateBeginGet, 0, 2); $i <= (int) substr($dateEndGet, 0, 2); $i++) {

                    $day = ($i < 10) ? '0' . $i : $i;
                    $dataInicio = substr($dateBeginGet, 6, 4) . '-' . substr($dateBeginGet, 3, 2) . '-' . $day;
                    $dataFim = substr($dateBeginGet, 6, 4) . '-' . substr($dateBeginGet, 3, 2) . '-' . $day;

                    $receitas[] = $this->__querySumPerConsultantGraph($this->request->getData('consultores'), $dataInicio, $dataFim, $arrayDefault);
                    $periodos[] = $day . '/' . $arrayMonth[(int) substr($dateBeginGet, 3, 2)];
                }
            }
            $this->set(compact('receitas', 'consultores', 'periodos'));
        }
    }

    public function pizza() {
        if ($this->request->is('post')) {
            $caoFaturaTable = TableRegistry::get('CaoFatura');
            $query = $caoFaturaTable->query();
            $query = $query->select([
                        'receita_liquida' => $query->func()->sum('CaoFatura.valor - (CaoFatura.valor * (CaoFatura.total_imp_inc/100))'),
                        'nome_do_consultor' => 'CaoUsuario.no_usuario'
                    ])
                    ->join([
                        'CaoOs' => [
                            'table' => 'cao_os',
                            'type' => 'INNER',
                            'conditions' => 'CaoFatura.co_os = CaoOs.co_os'
                        ],
                        'CaoUsuario' => [
                            'table' => 'cao_usuario',
                            'type' => 'INNER',
                            'conditions' => 'CaoUsuario.co_usuario = CaoOs.co_usuario'
                        ]
                    ])
                    ->where([
                        'CaoOs.co_usuario IN' => $this->request->getData('consultores'),
                        'CaoFatura.data_emissao >=' => implode('-', array_reverse(explode('/', $this->request->getData('data_inicio')))),
                        'CaoFatura.data_emissao <=' => implode('-', array_reverse(explode('/', $this->request->getData('data_fim'))))
                    ])
                    ->contain([])
                    ->group('CaoOs.co_usuario')
                    ->having(['receita_liquida >' => 0]);
            $faturaPorConsultores = $query->toArray();
            $this->set(compact('faturaPorConsultores'));
        }
    }

    private function __formatArrayReport($array, $salarios) {
        $newArray = [];
        foreach ($array as $key => $value) {
            $value['custo_fixo'] = (array_key_exists($value['consultor_id'], $salarios)) ? $salarios[$value['consultor_id']] : 0;
            $newArray[$value['consultor_id']] = ['receita_liquida' => $value['receita_liquida'], 'custo_fixo' => $value['custo_fixo'], 'comissao' => $value['comissao'], 'lucro' => ($value['receita_liquida'] - ($value['custo_fixo'] + $value['comissao']))];
        }
        return $newArray;
    }

    private function __querySumPerConsultantReport($consultores, $dataInicio, $dataFim, $arrayDefault) {

        /** Busca o salário que é utilizado como CUSTO FIXO. Os consultores que não possuírem salário terão o valor 
         * ZERO atribuído. */
        $caoSalarioTable = TableRegistry::get('CaoSalario');
        $query = $caoSalarioTable->query();
        $query = $query->find('list', [
                    'keyField' => 'co_usuario',
                    'valueField' => 'brut_salario'
                ])
                ->where([
                    'CaoSalario.co_usuario IN' => $this->request->getData('consultores'),
                ])
                ->contain([])
                ->order(['CaoSalario.co_usuario' => 'ASC']);
        $salarios = $query->toArray();

        $caoFaturaTable = TableRegistry::get('CaoFatura');
        $query = $caoFaturaTable->query();
        $query = $query->select([
                    'receita_liquida' => $query->func()->sum('CaoFatura.valor - (CaoFatura.valor * (CaoFatura.total_imp_inc/100))'),
                    'comissao' => $query->func()->sum('((CaoFatura.comissao_cn/100) * (CaoFatura.valor - (CaoFatura.valor * (CaoFatura.total_imp_inc/100))))'),
                    'consultor_id' => 'CaoUsuario.co_usuario'
                ])
                ->join([
                    'CaoOs' => [
                        'table' => 'cao_os',
                        'type' => 'INNER',
                        'conditions' => 'CaoFatura.co_os = CaoOs.co_os'
                    ],
                    'CaoUsuario' => [
                        'table' => 'cao_usuario',
                        'type' => 'INNER',
                        'conditions' => 'CaoUsuario.co_usuario = CaoOs.co_usuario'
                    ],
                ])
                ->where([
                    'CaoOs.co_usuario IN' => $consultores,
                    'CaoFatura.data_emissao >=' => implode('-', array_reverse(explode('/', $dataInicio))),
                    'CaoFatura.data_emissao <=' => implode('-', array_reverse(explode('/', $dataFim)))
                ])
                ->contain([])
                ->group('CaoOs.co_usuario');

        $replacements = $this->__formatArrayReport($query->toArray(), $salarios);
        return array_replace($arrayDefault, $replacements);
    }

    private function __formatArrayGraph($array, $salarios) {
        $newArray = [];
        foreach ($array as $key => $value) {
            $value['custo_fixo'] = (array_key_exists($value['consultor_id'], $salarios)) ? $salarios[$value['consultor_id']] : 0;
            $newArray[$value['consultor_id']] = ['receita_liquida' => $value['receita_liquida'], 'custo_fixo' => $value['custo_fixo']];
        }
        return $newArray;
    }

    private function __querySumPerConsultantGraph($consultores, $dataInicio, $dataFim, $arrayDefault) {
        
        /** Busca o salário que é utilizado como CUSTO FIXO. Os consultores que não possuírem salário terão o valor 
         * ZERO atribuído. */
        $caoSalarioTable = TableRegistry::get('CaoSalario');
        $query = $caoSalarioTable->query();
        $query = $query->find('list', [
                    'keyField' => 'co_usuario',
                    'valueField' => 'brut_salario'
                ])
                ->where([
                    'CaoSalario.co_usuario IN' => $this->request->getData('consultores'),
                ])
                ->contain([])
                ->order(['CaoSalario.co_usuario' => 'ASC']);
        $salarios = $query->toArray();        

        $caoFaturaTable = TableRegistry::get('CaoFatura');
        $query = $caoFaturaTable->query();
        $query = $query->select([
                    'receita_liquida' => $query->func()->sum('CaoFatura.valor - (CaoFatura.valor * (CaoFatura.total_imp_inc/100))'),
                    'consultor_id' => 'CaoUsuario.co_usuario'
                ])
                ->join([
                    'CaoOs' => [
                        'table' => 'cao_os',
                        'type' => 'INNER',
                        'conditions' => 'CaoFatura.co_os = CaoOs.co_os'
                    ],
                    'CaoUsuario' => [
                        'table' => 'cao_usuario',
                        'type' => 'INNER',
                        'conditions' => 'CaoUsuario.co_usuario = CaoOs.co_usuario'
                    ],
                ])
                ->where([
                    'CaoOs.co_usuario IN' => $consultores,
                    'CaoFatura.data_emissao >=' => implode('-', array_reverse(explode('/', $dataInicio))),
                    'CaoFatura.data_emissao <=' => implode('-', array_reverse(explode('/', $dataFim)))
                ])
                ->contain([])
                ->group('CaoOs.co_usuario');
        
        $replacements = $this->__formatArrayGraph($query->toArray(), $salarios);
        return array_replace($arrayDefault, $replacements);
    }

}
