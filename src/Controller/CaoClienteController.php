<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * CaoCliente Controller
 *
 * @property \App\Model\Table\CaoClienteTable $CaoCliente
 *
 * @method \App\Model\Entity\CaoCliente[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CaoClienteController extends AppController {

    public function reports() {
        $caocliente = $this->CaoCliente->find('list', ['limit' => 200, 'conditions' => ['CaoCliente.tp_cliente' => 'A']]);
        $this->set(compact('caocliente'));
    }

    public function report() {
         if ($this->request->is('post')) {

            $dateBeginGet = $this->request->getData('data_inicio');
            $dateEndGet = $this->request->getData('data_fim');

            /** Array com nome dos meses do ano, para formatar array. */
            $arrayMonth = [
                1 => 'Jan',
                2 => 'Fev',
                3 => 'Mar',
                4 => 'Abr',
                5 => 'Mai',
                6 => 'Jun',
                7 => 'Jul',
                8 => 'Ago',
                9 => 'Set',
                10 => 'Out',
                11 => 'Nov',
                12 => 'Dez'
            ];

            $auxMonthBegin = substr($dateBeginGet, 3, 2);
            $auxMonthEnd = substr($dateEndGet, 3, 2);

            $keys = $this->request->getData('clientes');
            $arrayDefault = array_fill_keys($keys, 0);

            $query = $this->CaoCliente
                    ->find('list', [
                        'keyField' => 'co_cliente',
                        'valueField' => 'no_fantasia'
                    ])
                    ->where([
                        'CaoCliente.co_cliente IN' => $this->request->getData('clientes'),
                    ])
                    ->contain([])
                    ->order(['CaoCliente.co_cliente' => 'ASC']);
            $clientes = $query->toArray();

            if (($auxMonthBegin != $auxMonthEnd) || (substr($dateBeginGet, 6, 4) != substr($dateEndGet, 6, 4))) {

                /** Se for de anos diferentes, então, percorre 2 for's: Primeiro até o fim do ano da primeira data e outro do mês 01
                 * até o mês do ano da segunda data. */
                if (substr($dateBeginGet, 6, 4) != substr($dateEndGet, 6, 4)) {

                    /** Percorre os meses começando no mês da primeira data até Dezembro. */
                    for ($i = (int) $auxMonthBegin; $i <= 12; $i++) {

                        if ($i == $auxMonthBegin) {
                            $day = substr($dateBeginGet, 0, 2);
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateBeginGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = $year . '-' . $month . '-' . substr($dateBeginGet, 0, 2);
                        } else {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateBeginGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year));
                        }
                        $receitas[] = $this->__querySumPerCustomer($this->request->getData('clientes'), $dataInicio, $dataFim, $arrayDefault);
                        $periodos[] = $arrayMonth[$i] . '/' . $year;
                    }

                    /** Percorre os anos intermediários das duas datas de Janeiro atẽ Dezembro. */
                    $nextYear = ((int) substr($dateBeginGet, 6, 4)) + 1;
                    $lastYear = ((int) substr($dateEndGet, 6, 4)) - 1;
                    for ($j = $nextYear; $j <= $lastYear; $j++) {
                        for ($i = 1; $i <= 12; $i++) {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = $j;
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year));

                            $receitas[] = $this->__querySumPerCustomer($this->request->getData('clientes'), $dataInicio, $dataFim, $arrayDefault);
                            $periodos[] = $arrayMonth[$i] . '/' . $year;
                        }
                    }

                    /** Percorre começando de Janeiro até o mês final correspondente a segunda data. */
                    for ($i = 1; $i <= (int) $auxMonthEnd; $i++) {

                        if ($i == $auxMonthEnd) {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateEndGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = $year . '-' . $month . '-' . substr($dateEndGet, 0, 2);
                        } else {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateEndGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year));
                        }
                        $receitas[] = $this->__querySumPerCustomer($this->request->getData('clientes'), $dataInicio, $dataFim, $arrayDefault);
                        $periodos[] = $arrayMonth[$i] . '/' . $year;
                    }
                } else {
                    /** Se for de anos iguais, então, percorre contando do mês referente a primeira data até o mês referente a
                     * segunda data. */
                    for ($i = (int) $auxMonthBegin; $i <= (int) $auxMonthEnd; $i++) {

                        if ($i == (int) $auxMonthBegin) {
                            $day = substr($dateBeginGet, 0, 2);
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateBeginGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day . ' 00:00';
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year)) . ' 23:59';
                        } else if ($i == (int) $auxMonthEnd) {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateEndGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day . ' 00:00';
                            $dataFim = $year . '-' . $month . '-' . substr($dateEndGet, 0, 2) . ' 23:59';
                        } else {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateBeginGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day . ' 00:00';
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year)) . ' 23:59';
                        }
                        $receitas[] = $this->__querySumPerCustomer($this->request->getData('clientes'), $dataInicio, $dataFim, $arrayDefault);
                        $periodos[] = $arrayMonth[$i] . '/' . $year;
                    }
                }
            } else {
                /** Se for do mesmo mês e ano. */
                for ($i = (int) substr($dateBeginGet, 0, 2); $i <= (int) substr($dateEndGet, 0, 2); $i++) {

                    $day = ($i < 10) ? '0' . $i : $i;
                    $dataInicio = substr($dateBeginGet, 6, 4) . '-' . substr($dateBeginGet, 3, 2) . '-' . $day;
                    $dataFim = substr($dateBeginGet, 6, 4) . '-' . substr($dateBeginGet, 3, 2) . '-' . $day;

                    $receitas[] = $this->__querySumPerCustomer($this->request->getData('clientes'), $dataInicio, $dataFim, $arrayDefault);
                    $periodos[] = $day . '/' . $arrayMonth[(int) substr($dateBeginGet, 3, 2)];
                }
            }
            $this->set(compact('receitas', 'clientes', 'periodos'));
        }
    }

    public function graph() {
        if ($this->request->is('post')) {

            $dateBeginGet = $this->request->getData('data_inicio');
            $dateEndGet = $this->request->getData('data_fim');

            /** Array com nome dos meses do ano, para formatar array. */
            $arrayMonth = [
                1 => 'Jan',
                2 => 'Fev',
                3 => 'Mar',
                4 => 'Abr',
                5 => 'Mai',
                6 => 'Jun',
                7 => 'Jul',
                8 => 'Ago',
                9 => 'Set',
                10 => 'Out',
                11 => 'Nov',
                12 => 'Dez'
            ];

            $auxMonthBegin = substr($dateBeginGet, 3, 2);
            $auxMonthEnd = substr($dateEndGet, 3, 2);

            $keys = $this->request->getData('clientes');
            $arrayDefault = array_fill_keys($keys, 0);

            $query = $this->CaoCliente
                    ->find('list', [
                        'keyField' => 'co_cliente',
                        'valueField' => 'no_fantasia'
                    ])
                    ->where([
                        'CaoCliente.co_cliente IN' => $this->request->getData('clientes'),
                    ])
                    ->contain([])
                    ->order(['CaoCliente.co_cliente' => 'ASC']);
            $clientes = $query->toArray();

            if (($auxMonthBegin != $auxMonthEnd) || (substr($dateBeginGet, 6, 4) != substr($dateEndGet, 6, 4))) {

                /** Se for de anos diferentes, então, percorre 2 for's: Primeiro até o fim do ano da primeira data e outro do mês 01
                 * até o mês do ano da segunda data. */
                if (substr($dateBeginGet, 6, 4) != substr($dateEndGet, 6, 4)) {

                    /** Percorre os meses começando no mês da primeira data até Dezembro. */
                    for ($i = (int) $auxMonthBegin; $i <= 12; $i++) {

                        if ($i == $auxMonthBegin) {
                            $day = substr($dateBeginGet, 0, 2);
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateBeginGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = $year . '-' . $month . '-' . substr($dateBeginGet, 0, 2);
                        } else {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateBeginGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year));
                        }
                        $receitas[] = $this->__querySumPerCustomer($this->request->getData('clientes'), $dataInicio, $dataFim, $arrayDefault);
                        $periodos[] = $arrayMonth[$i] . '/' . $year;
                    }

                    /** Percorre os anos intermediários das duas datas de Janeiro atẽ Dezembro. */
                    $nextYear = ((int) substr($dateBeginGet, 6, 4)) + 1;
                    $lastYear = ((int) substr($dateEndGet, 6, 4)) - 1;
                    for ($j = $nextYear; $j <= $lastYear; $j++) {
                        for ($i = 1; $i <= 12; $i++) {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = $j;
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year));

                            $receitas[] = $this->__querySumPerCustomer($this->request->getData('clientes'), $dataInicio, $dataFim, $arrayDefault);
                            $periodos[] = $arrayMonth[$i] . '/' . $year;
                        }
                    }

                    /** Percorre começando de Janeiro até o mês final correspondente a segunda data. */
                    for ($i = 1; $i <= (int) $auxMonthEnd; $i++) {

                        if ($i == $auxMonthEnd) {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateEndGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = $year . '-' . $month . '-' . substr($dateEndGet, 0, 2);
                        } else {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateEndGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day;
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year));
                        }
                        $receitas[] = $this->__querySumPerCustomer($this->request->getData('clientes'), $dataInicio, $dataFim, $arrayDefault);
                        $periodos[] = $arrayMonth[$i] . '/' . $year;
                    }
                } else {
                    /** Se for de anos iguais, então, percorre contando do mês referente a primeira data até o mês referente a
                     * segunda data. */
                    for ($i = (int) $auxMonthBegin; $i <= (int) $auxMonthEnd; $i++) {

                        if ($i == (int) $auxMonthBegin) {
                            $day = substr($dateBeginGet, 0, 2);
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateBeginGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day . ' 00:00';
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year)) . ' 23:59';
                        } else if ($i == (int) $auxMonthEnd) {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateEndGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day . ' 00:00';
                            $dataFim = $year . '-' . $month . '-' . substr($dateEndGet, 0, 2) . ' 23:59';
                        } else {
                            $day = '01';
                            $month = ($i < 10) ? '0' . $i : $i;
                            $year = substr($dateBeginGet, 6, 4);
                            $dataInicio = $year . '-' . $month . '-' . $day . ' 00:00';
                            $dataFim = date('Y-m-d', mktime(0, 0, 0, ($i + 1), 0, $year)) . ' 23:59';
                        }
                        $receitas[] = $this->__querySumPerCustomer($this->request->getData('clientes'), $dataInicio, $dataFim, $arrayDefault);
                        $periodos[] = $arrayMonth[$i] . '/' . $year;
                    }
                }
            } else {
                /** Se for do mesmo mês e ano. */
                for ($i = (int) substr($dateBeginGet, 0, 2); $i <= (int) substr($dateEndGet, 0, 2); $i++) {

                    $day = ($i < 10) ? '0' . $i : $i;
                    $dataInicio = substr($dateBeginGet, 6, 4) . '-' . substr($dateBeginGet, 3, 2) . '-' . $day;
                    $dataFim = substr($dateBeginGet, 6, 4) . '-' . substr($dateBeginGet, 3, 2) . '-' . $day;

                    $receitas[] = $this->__querySumPerCustomer($this->request->getData('clientes'), $dataInicio, $dataFim, $arrayDefault);
                    $periodos[] = $day . '/' . $arrayMonth[(int) substr($dateBeginGet, 3, 2)];
                }
            }
            $this->set(compact('receitas', 'clientes', 'periodos'));
        }
    }

    public function pizza() {
        if ($this->request->is('post')) {
            $query = $this->CaoCliente->CaoFatura->find();
            $query = $query->select([
                        'receita_liquida' => $query->func()->sum('CaoFatura.valor - (CaoFatura.valor * (CaoFatura.total_imp_inc/100))'),
                        'CaoFatura.co_cliente',
                        'CaoCliente.no_fantasia'
                    ])
                    ->where([
                        'CaoFatura.co_cliente IN' => $this->request->getData('clientes'),
                        'CaoFatura.data_emissao >=' => implode('-', array_reverse(explode('/', $this->request->getData('data_inicio')))),
                        'CaoFatura.data_emissao <=' => implode('-', array_reverse(explode('/', $this->request->getData('data_fim'))))
                    ])
                    ->contain(['CaoCliente'])
                    ->group('CaoFatura.co_cliente')
                    ->having(['receita_liquida >' => 0]);
            $faturaPorClientes = $query->toArray();
            $this->set(compact('faturaPorClientes'));
        }
    }

    private function __formatArray($array) {
        $newArray = [];
        foreach ($array as $key => $value) {
            $newArray[$value['cliente_id']] = $value['receita_liquida'];
        }
        return $newArray;
    }

    private function __querySumPerCustomer($clientes, $dataInicio, $dataFim, $arrayDefault) {
        $query = $this->CaoCliente->CaoFatura->find();
        $query = $query->select([
                    'receita_liquida' => $query->func()->sum('CaoFatura.valor - (CaoFatura.valor * (CaoFatura.total_imp_inc/100))'),
                    'cliente_id' => 'CaoFatura.co_cliente',
                ])
                ->where([
                    'CaoFatura.co_cliente IN' => $clientes,
                    'CaoFatura.data_emissao >=' => $dataInicio,
                    'CaoFatura.data_emissao <=' => $dataFim
                ])
                ->contain(['CaoCliente'])
                ->group('CaoFatura.co_cliente');

        $replacements = $this->__formatArray($query->toArray());
        return array_replace($arrayDefault, $replacements);
    }

}
