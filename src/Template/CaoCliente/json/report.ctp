<?php

$json['table'] = NULL;
$totalPorCliente = [];
$auxTotalPorCliente = '<tr><td>TOTAL</td>';
foreach ($receitas as $key => $periodo) {
    $json['table'] .= '<tr><td>' . $periodos[$key] . '</td>';
    $totalPorPeriodo[$key] = 0;
    $campeaoDoPeriodo = NULL;
    $valorDoCampeao = 0;
    foreach ($periodo as $key2 => $receitaPorCliente) {
        if ($receitaPorCliente > $valorDoCampeao) {
            $campeaoDoPeriodo = $key2;
            $valorDoCampeao = $receitaPorCliente;
        }
        if (!isset($totalPorCliente[$key2])) {
            $totalPorCliente[$key2] = 0;
        }
        $totalPorCliente[$key2] = $totalPorCliente[$key2] + $receitaPorCliente;
        $totalPorPeriodo[$key] = $totalPorPeriodo[$key] + $receitaPorCliente;
        $auxTotalPorPeriodo[$key2] = '<td>' . $this->Number->format($receitaPorCliente, ['places' => 2, 'before' => 'R$ ', 'locale' => 'pt_BR']) . '</td>';
    }
    if ($campeaoDoPeriodo != NULL) {
        $auxTotalPorPeriodo[$campeaoDoPeriodo] = str_replace(['<td>', '</td>'], ['<td class="text-info"><span class="badge badge-info">', '</span></td>'], $auxTotalPorPeriodo[$campeaoDoPeriodo]);
    }
    $json['table'] .= implode('', $auxTotalPorPeriodo);
    $json['table'] .= '<td>' . $this->Number->format($totalPorPeriodo[$key], ['places' => 2, 'before' => 'R$ ', 'locale' => 'pt_BR']) . '</td>';
    $json['table'] .= '</tr>';
}

$campeaoDoPeriodo = NULL;
$valorDoCampeao = 0;
$tableHead = '<div class="table-responsive noSwipe"><table class="table table-sm table-striped"><thead><tr><th>Período</th>';
$json['table'] .= '<tr class="table-secondary"><td>TOTAL</td>';
foreach ($clientes as $key => $cliente) {
    if ($totalPorCliente[$key] > $valorDoCampeao) {
        $campeaoDoPeriodo = $key2;
        $valorDoCampeao = $totalPorCliente[$key];
    }
    $auxTotalPorPeriodo[$key] = '<td>' . $this->Number->format($totalPorCliente[$key], ['places' => 2, 'before' => 'R$ ', 'locale' => 'pt_BR']) . '</td>';
    $tableHead .= '<th>' . $cliente . '</th>';
}
$tableHead .= '<th>TOTAL</th></tr></thead><tbody>';
if ($campeaoDoPeriodo != NULL) {
    $auxTotalPorPeriodo[$campeaoDoPeriodo] = str_replace(['<td>', '</td>'], ['<td class="text-info"><span class="badge badge-info">', '</span></td>'], $auxTotalPorPeriodo[$campeaoDoPeriodo]);
}
$json['table'] .= implode('', $auxTotalPorPeriodo);
$json['table'] .= '<td style="font-weight: bold;">' . $this->Number->format(array_sum($totalPorCliente), ['places' => 2, 'before' => 'R$ ', 'locale' => 'pt_BR']) . '</td></tr>';
$json['table'] .= '</tbody></table></div>';
$json['table'] = $tableHead . $json['table'];

echo json_encode($json);
?>
