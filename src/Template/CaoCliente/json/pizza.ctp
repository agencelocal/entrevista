<?php

$data = $backgroundColor = [];
foreach ($faturaPorClientes as $key => $fatura) {
    $json['labels'][] = $fatura['cao_cliente']['no_fantasia'];
    $data[] = round($fatura['receita_liquida'], 2);
    $backgroundColor[] = 'rgb(' . mt_rand(0, 255) . ', ' . mt_rand(0, 255) . ', ' . mt_rand(0, 255) . ')';
}
$json['datasets'][] = ['label' => 'Dataset 1', 'data' => $data, 'backgroundColor' => $backgroundColor];

echo json_encode($json);
?>