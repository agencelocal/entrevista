<?php

$json['labels'] = $periodos;

foreach ($clientes as $key => $cliente) {
    $data = [];
    foreach ($receitas as $key2 => $receita) {
        $data[] = round($receita[$key], 2);
    }
    $json['datasets'][] = ['label' => $cliente, 'data' => $data, 'fill' => false, 'borderColor' => 'rgb(' . mt_rand(0, 255) . ', ' . mt_rand(0, 255) . ', ' . mt_rand(0, 255) . ')', 'lineTension' => 0];
}

echo json_encode($json);
?>