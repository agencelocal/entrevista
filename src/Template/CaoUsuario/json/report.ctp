<?php

$json['table'] = NULL;
$table = $totalReceitaLiquida = $totalCustoFixo = $totalComissao = $totalLucro = array_fill_keys(array_keys($consultores), NULL);

foreach ($receitas as $key => $periodo) {
    foreach ($periodo as $key2 => $receitaPorConsultor) {
        if ($receitaPorConsultor['lucro'] < 0) {
            $classLucro = 'text-danger';
            $lucro = '- ' . $this->Number->format(round(($receitaPorConsultor['lucro'] * (-1)), 2), ['places' => 2, 'before' => 'R$ ', 'locale' => 'pt_BR']);
        } else {
            $classLucro = 'text-primary';
            $lucro = $this->Number->format(round($receitaPorConsultor['lucro'], 2), ['places' => 2, 'before' => 'R$ ', 'locale' => 'pt_BR']);
        }
        $table[$key2] .= '<tr><td>' . $periodos[$key] . '</td>';
        $table[$key2] .= '<td class="text-primary">' . $this->Number->format(round($receitaPorConsultor['receita_liquida'], 2), ['places' => 2, 'before' => 'R$ ', 'locale' => 'pt_BR']) . '</td>';
        $table[$key2] .= '<td class="text-danger">- ' . $this->Number->format(round($receitaPorConsultor['custo_fixo'], 2), ['places' => 2, 'before' => 'R$ ', 'locale' => 'pt_BR']) . '</td>';
        $table[$key2] .= '<td class="text-danger">- ' . $this->Number->format(round($receitaPorConsultor['comissao'], 2), ['places' => 2, 'before' => 'R$ ', 'locale' => 'pt_BR']) . '</td>';
        $table[$key2] .= '<td class="' . $classLucro . '">' . $lucro . '</td>';
        $table[$key2] .= '</tr>';
        $totalReceitaLiquida[$key2] = $totalReceitaLiquida[$key2] + $receitaPorConsultor['receita_liquida'];
        $totalCustoFixo[$key2] = $totalCustoFixo[$key2] + $receitaPorConsultor['custo_fixo'];
        $totalComissao[$key2] = $totalComissao[$key2] + $receitaPorConsultor['comissao'];
        $totalLucro[$key2] = $totalLucro[$key2] + $receitaPorConsultor['lucro'];
    }
}

foreach ($consultores as $key => $consultor) {
    $json['table'] .= '<div class="table-responsive noSwipe">'
            . '<table class="table table-sm table-striped">'
            . '<tbody>'
            . '<tr class="table-info">'
            . '<td colspan="5" style="font-size: 14px;">' . $consultor . '</td>'
            . '</tr>'
            . '<tr class="table-secondary">'
            . '<td>Período</td>'
            . '<td>Receita Líquida</td>'
            . '<td>Custo Fixo</td>'
            . '<td>Comissão</td>'
            . '<td>Lucro</td>'
            . '</tr>'
            . $table[$key]
            . '<tr class="table-dark">'
            . '<td>SALDO</td>'
            . '<td>' . $this->Number->format(round($totalReceitaLiquida[$key], 2), ['places' => 2, 'before' => 'R$ ', 'locale' => 'pt_BR']) . '</td>'
            . '<td>- ' . $this->Number->format(round($totalCustoFixo[$key], 2), ['places' => 2, 'before' => 'R$ ', 'locale' => 'pt_BR']) . '</td>'
            . '<td>- ' . $this->Number->format(round($totalComissao[$key], 2), ['places' => 2, 'before' => 'R$ ', 'locale' => 'pt_BR']) . '</td>'
            . '<td>' . $this->Number->format(round($totalLucro[$key], 2), ['places' => 2, 'before' => 'R$ ', 'locale' => 'pt_BR']) . '</td>'
            . '</tr>'
            . '</tbody>'
            . '</table>'
            . '</div>'
            . '<br/><br/>';
}

echo json_encode($json);
?>
