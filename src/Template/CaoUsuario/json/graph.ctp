<?php

$json['labels'] = $periodos;
$custoFixoTotalPorPeriodo = array_fill_keys(array_keys($receitas), 0);
foreach ($consultores as $key => $consultor) {
    $dataBar = [];
    foreach ($receitas as $key2 => $receita) {
        $dataBar[] = round($receita[$key]['receita_liquida'], 2);
        $custoFixoTotalPorPeriodo[$key2] = $custoFixoTotalPorPeriodo[$key2] + $receita[$key]['custo_fixo'];
    }
    $json['datasets'][] = ['type' => 'bar', 'label' => $consultor, 'data' => $dataBar, 'backgroundColor' => 'rgb(' . mt_rand(0, 255) . ', ' . mt_rand(0, 255) . ', ' . mt_rand(0, 255) . ')', 'borderWidth' => 2, 'borderColor' => 'white'];
}

$totalConsultores = count($consultores);
foreach ($receitas as $key2 => $receita) {
    $dataLine[] = $custoFixoTotalPorPeriodo[$key2] / $totalConsultores;
}
$json['datasets'][] = ['type' => 'line', 'label' => 'Custo Médio', 'data' => $dataLine, 'fill' => false, 'borderWidth' => 2, 'borderColor' => 'blue', 'lineTension' => 0];

echo json_encode($json);
?>