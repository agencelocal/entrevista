<?php

$data = $backgroundColor = [];
foreach ($faturaPorConsultores as $key => $fatura) {
    $json['labels'][] = $fatura['nome_do_consultor'];
    $data[] = round($fatura['receita_liquida'], 2);
    $backgroundColor[] = 'rgb(' . mt_rand(0, 255) . ', ' . mt_rand(0, 255) . ', ' . mt_rand(0, 255) . ')';
}
$json['datasets'][] = ['label' => 'Dataset 1', 'data' => $data, 'backgroundColor' => $backgroundColor];

echo json_encode($json);
?>