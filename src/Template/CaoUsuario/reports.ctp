<?php
echo $this->Html->script(['CaoUsuario/reports.js'], ['block' => 'script']);
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-border-color panel-border-color-primary">
            <div class="panel-heading panel-heading-divider">Relatórios Por Consultor<span class="panel-subtitle">1. Selecione os consultores e o período desejado.</span></div>
            <div class="panel-body">
                <form id="form-filter">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Consultores</label>
                            <?php
                            echo $this->Form->control('consultores', ['options' => $consultores, 'style' => 'width: 100%;', 'multiple' => 'multiple', 'label' => false, 'div' => false, 'class' => 'form-control']);
                            ?>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Data Início</label>
                            <?php
                            echo $this->Form->control('data_inicio', ['value' => $this->request->getQuery('data-inicio'), 'label' => false, 'div' => false, 'class' => 'form-control form-control-xs datepicker']);
                            ?>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Data Fim</label>
                            <?php
                            echo $this->Form->control('data_fim', ['value' => $this->request->getQuery('data-fim'), 'label' => false, 'div' => false, 'class' => 'form-control form-control-xs datepicker']);
                            ?>
                        </div>
                    </div>
                </form>
                <br/><br/>
                <span class="panel-subtitle">2. Clique no botão do relatório desejado.</span><hr />
                <div class="row" style="margin-top: 20px;">
                    <div class="col-md-4">
                        <button style="width: 100%;" id="btn-report" onclick="ajaxReport('relatório');" type="button" class="btn-report btn btn-space btn-primary btn-big"><i class="icon s7-display2"></i> Receita Detalhada</button>
                    </div>
                    <div class="col-md-4">
                        <button style="width: 100%;" id="btn-graph" onclick="ajaxReport('gráfico');" type="button" class="btn-report btn btn-space btn-primary btn-big"><i class="icon icon-left s7-graph2"></i> Desempenho e Custo Médio</button>
                    </div>
                    <div class="col-md-4">
                        <button style="width: 100%;" id="btn-pizza" onclick="ajaxReport('pizza');" type="button" class="btn-report btn btn-space btn-primary btn-big"><i class="icon icon-left s7-graph"></i> Percentual de Receita</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="widget widget-fullwidth">
            <div id="title-report" class="widget-head" style="display: none;">
                <div class="tools"><span data-toggle="tooltip" data-placement="top" title="Imprimir Relatório" data-original-title="Atualizar Relatório" class="icon s7-print" onclick=""></span></div>
                <span id="description-main-report" class="title"></span><span id="description-report" class="description"></span>
            </div>
            <div id="loading-report" style="display: none; padding-left: 30%;">
                <h6 class="mb-4" style="color: #36cb8f; font-weight: bold;"><?= $this->Html->image('gif/Spinner-1s-200px.gif', ['alt' => 'Carregando', 'style' => 'height: 80px; width: 80px;']); ?>Carregando...</h6>
            </div>
            <div id="content-report" class="widget-chart-container">

            </div>
        </div>
    </div>
</div>
<input type="hidden" id="csrfToken" value="<?=$this->request->getParam('_csrfToken') ?>" />