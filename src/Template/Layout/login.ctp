<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            AtiveCRM
        </title>
        <?= $this->Html->meta('favicon.png', '/img/favicon/favicon.png', ['type' => 'icon']); ?>
        <?= $this->Html->css('style.css') ?>
        <?= $this->Html->css('perfect-scrollbar.min.css') ?>
        <?= $this->Html->css('select2/dist/css/select2.min.css') ?>
        <?= $this->Html->css('app.css') ?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
    </head>
    <body class="mai-splash-screen">
        <div class="mai-wrapper mai-login">
            <div class="main-content container">
                <div class="splash-container row">
                    <div class="col-md-6 user-message">
                        <span class="splash-message text-right">Plataforma CRM<br>A ferramenta para aprimorar<br>as vendas do seu negócio</span>
                        <span class="alternative-message text-right">Não possui uma conta? 
                            <a href="#" data-toggle="modal" data-target="#md-fale-conosco" class="md-trigger">Fale Conosco</a>
                        </span>
                    </div>
                    <div class="col-md-6 form-message">
                        <div class="text-center" style="font-size: 28px; font-weight: bold;">AtiveCRM</div>
                        <!--<img src="" alt="logo" width="169" height="28" class="logo-img mb-4">-->
                        <?php
                        echo $this->fetch('content');
                        ?>
                        <div class="out-links"><a href="javascript:void(0)">© 2018 Todos os direitos reservados</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="md-fale-conosco" role="dialog" class="modal modal-full-color modal-full-color-primary fade" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" class="close"><span class="s7-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <div class="text-primary"><span style="color: #fff;" class="modal-main-icon s7-headphones"></span></div>
                            <h3 class="mb-4">Canais de Atendimento:</h3>
                            <p>Email: suporte@ativecrm.com.br<br></p>
                            <p>Telefone/WhatsApp: (67) 98429-5618<br></p>
                            <div class="mt-6">
                                <button type="button" data-dismiss="modal" class="btn btn-sm btn-space btn-primary">Fechar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        <?php
        echo $this->Html->script('jquery-3.3.1.min.js');
        echo $this->Html->script('perfect-scrollbar.jquery.min.js');
        echo $this->Html->script('bootstrap.bundle.min.js');
        echo $this->Html->script('app.js');
        echo $this->Html->script('jQuery.countdown/dist/jquery.countdown.min.js');
        echo $this->Html->script('select2/dist/js/select2.full.js');
        echo $this->Html->script('select2/dist/js/i18n/pt-BR.js');
        echo $this->Html->script('jQuery-Mask-Plugin/src/jquery.mask.js');
        echo $this->Html->script('jquery-validation/src/core.js');
        echo $this->Html->script('jquery-validation/src/default-settings.js');
        echo $this->Html->script('jquery-validation/src/localization/messages_pt_BR.js');
        echo $this->fetch('script');
        ?>
        <script type="text/javascript">
            $(document).ready(function () {
                //initialize the javascript
                App.init();
            });
        </script>
    </body>
</html>
