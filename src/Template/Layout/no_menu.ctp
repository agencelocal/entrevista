<?php

?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            Y Ative CRM
        </title>
        <?= $this->Html->meta('icon') ?>

        <?= $this->Html->css('style.css') ?>
        <?= $this->Html->css('perfect-scrollbar.min.css') ?>
        <?= $this->Html->css('app.css') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>
        <nav class="navbar navbar-expand navbar-dark mai-top-header">
            <div class="container"><a href="#" class="navbar-brand"></a>
                <!--Left Menu-->
                <ul class="nav navbar-nav mai-top-nav">
                </ul>
                <!--Icons Menu-->
                <ul class="navbar-nav float-lg-right mai-icons-nav">
                    <li class="dropdown nav-item mai-notifications"><a href="#" data-toggle="dropdown" role="button" class="nav-link dropdown-toggle"><span class="icon s7-bell"></span><span class="indicator"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="title">Notifications</div>
                                <div class="mai-scroller ps-container ps-theme-default" data-ps-id="39a883a8-e639-2791-816a-6d0813b2a265">
                                    <div class="content">
                                        <ul>
                                            <li><a href="#">
                                                    <div class="icon"><span class="s7-check"></span></div>
                                                    <div class="content"><span class="desc">This is a new message for my dear friend <strong>Rob</strong>.</span><span class="date">10 minutes ago</span></div></a></li>
                                            <li><a href="#">
                                                    <div class="icon"><span class="s7-add-user"></span></div>
                                                    <div class="content"><span class="desc">You have a new fiend request pending from <strong>John Doe</strong>.</span><span class="date">2 days ago</span></div></a></li>
                                            <li><a href="#">
                                                    <div class="icon"><span class="s7-graph1"></span></div>
                                                    <div class="content"><span class="desc">Your site visits have increased <strong>15.5%</strong> more since the last week.</span><span class="date">2 hours ago</span></div></a></li>
                                            <li><a href="#">
                                                    <div class="icon"><span class="s7-check"></span></div>
                                                    <div class="content"><span class="desc">This is a new message for my dear friend <strong>Rob</strong>.</span><span class="date">10 minutes ago</span></div></a></li>
                                        </ul>
                                    </div>
                                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
                                <div class="footer"> <a href="#">View all notifications</a></div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!--User Menu-->
                <ul class="nav navbar-nav float-lg-right mai-user-nav">
                    <li class="dropdown nav-item">
                        <a href="#" data-toggle="dropdown" role="button" class="dropdown-toggle nav-link"> 
                            <span class="user-name">Francys Reymer</span>
                            <span class="angle-down s7-angle-down"></span>
                        </a>
                        <div role="menu" class="dropdown-menu">
                            <a href="/etapa-vendas" class="dropdown-item"><span class="icon s7-filter"> </span>Etapas de Venda</a>
                            <a href="/produto-servicos" class="dropdown-item"><span class="icon s7-box1"> </span>Produtos/Serviços</a>
                            <a href="/categoria-clientes" class="dropdown-item"> <span class="icon s7-network"> </span>Categorias dos Clientes</a>
                            <a href="/users" class="dropdown-item"> <span class="icon s7-user"> </span>Usuários</a>
                            <a href="/sair" class="dropdown-item"><span class="icon s7-power"> </span>Sair</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="mai-wrapper">
            <div class="main-content container">
                 <?= $this->Flash->render() ?>
                <?= $this->fetch('content') ?>
            </div>
        </div>
        <footer>
        </footer>
        <?php
echo $this->Html->script('jquery.min.js'); 
echo $this->Html->script('perfect-scrollbar.jquery.min.js');
echo $this->Html->script('bootstrap.bundle.min.js');
echo $this->Html->script('app.js');

        
        echo $this->fetch('script');        
        
        ?>
<script type="text/javascript">
            $(document).ready(function () {
                //initialize the javascript
                App.init();
            });

        </script>        
    </body>
</html>
