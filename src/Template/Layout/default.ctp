<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            Agence Teste
        </title>
        <?= $this->Html->meta('favicon.png', '/img/favicon/favicon.png', ['type' => 'icon']); ?>
        <?= $this->Html->css('style.css') ?>
        <?= $this->Html->css('perfect-scrollbar.min.css') ?>
        <?= $this->Html->css('select2/dist/css/select2.min.css') ?>
        <?= $this->Html->css('fuelux/dist/css/fuelux.min.css') ?>
        <?= $this->Html->css('bootstrap-slider.min.css') ?>
        <?= $this->Html->css('app.css') ?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
    </head>
    <body>
        <nav class="navbar navbar-expand navbar-dark mai-top-header">
            <div class="container"><a href="/" class="navbar-brand" style="width: 100px; font-weight: bold;">AgenceAdmin</a>
                <!--Left Menu-->
                <ul class="nav navbar-nav mai-top-nav">
                    <li class="nav-item"><a href="#" data-toggle="modal" data-target="#md-default" class="nav-link">Suporte</a></li>
                </ul>
                <!--Icons Menu-->
                <ul class="navbar-nav float-lg-right mai-icons-nav">
                    <li class="dropdown nav-item mai-notifications"><a href="#" data-toggle="dropdown" role="button" class="nav-link dropdown-toggle"><span class="icon s7-bell"></span><span class="indicator"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="title">Notificações</div>
                                <div class="mai-scroller ps-container ps-theme-default" data-ps-id="39a883a8-e639-2791-816a-6d0813b2a265">
                                    <div class="content">
                                        <ul id="list-notifications"></ul>
                                    </div>
                                    <div class="ps-scrollbar-x-rail" style="display: none;left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
                                <div class="footer" style="display: none;"><a href="#">View all notifications</a></div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!--User Menu-->
                <ul class="nav navbar-nav float-lg-right mai-user-nav">
                    <li class="dropdown nav-item">
                        <a href="#" data-toggle="dropdown" role="button" class="dropdown-toggle nav-link"> 
                            <span class="user-name">
                                <?php
                                echo 'Carlos F. G. Arruda';
                                ?>
                            </span>
                            <span class="angle-down s7-angle-down"></span>
                        </a>
                        <div role="menu" class="dropdown-menu">
                            <a href="#" class="dropdown-item"> <span class="icon s7-user"> </span>Usuários</a>
                            <a href="#" class="dropdown-item"><span class="icon s7-power"> </span>Sair</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="mai-wrapper">
            <nav class="navbar navbar-expand-lg mai-sub-header">
                <div class="container">
                    <!-- Mega Menu structure-->
                    <nav class="navbar navbar-expand-md">
                        <button class="navbar-toggler hidden-md-up collapsed" type="button" data-toggle="collapse" data-target="#mai-navbar-collapse" aria-controls="#mai-navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                            <div class="icon-bar"><span></span><span></span><span></span></div>
                        </button>
                        <div id="mai-navbar-collapse" class="navbar-collapse collapse mai-nav-tabs">
                            <ul class="nav navbar-nav">
                                <li class="nav-item parent open"><a href="#" role="button" class="nav-link"><span class="icon s7-display1"></span><span>Relatórios</span></a>
                                    <ul class="mai-nav-tabs-sub mai-sub-nav nav">
                                        <li class="nav-item"><a href="<?= $this->Url->build(["controller" => "CaoCliente", "action" => "reports"]); ?>" class="nav-link <?=$classMenuPorCliente ?>"><span class="icon s7-diamond"></span><span class="name">Por Cliente</span></a>
                                        </li>                                        
                                        <li class="nav-item"><a href="<?= $this->Url->build(["controller" => "CaoUsuario", "action" => "reports"]); ?>" class="nav-link <?=$classMenuPorConsultor ?>"><span class="icon s7-id"></span><span class="name">Por Consultor</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item parent"><a href="#" role="button" class="nav-link"><span class="icon s7-diamond"></span><span>Clientes</span></a>
                                    <ul class="mai-nav-tabs-sub mai-sub-nav nav">
                                        <li class="nav-item"><a href="#" class="nav-link"><span class="icon s7-menu"></span><span class="name">Listar Clientes</span></a>
                                        </li>                                        
                                        <li class="nav-item"><a href="#" class="nav-link "><span class="icon s7-plus"></span><span class="name">Adicionar Novo</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item parent"><a href="#" role="button" class="nav-link"><span class="icon s7-id"></span><span>Consultores</span></a>
                                    <ul class="mai-nav-tabs-sub mai-sub-nav nav">
                                        <li class="nav-item"><a href="#" class="nav-link"><span class="icon s7-menu"></span><span class="name">Listar Consultores</span></a>
                                        </li>                                        
                                        <li class="nav-item"><a href="#" class="nav-link "><span class="icon s7-plus"></span><span class="name">Adicionar Novo</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item parent"><a href="#" role="button" class="nav-link"><span class="icon s7-rocket"></span><span>O.S.</span></a>
                                    <ul class="mai-nav-tabs-sub mai-sub-nav nav">
                                        <li class="nav-item"><a href="#" class="nav-link"><span class="icon s7-menu"></span><span class="name">Listar O.S.</span></a>
                                        </li>                                        
                                        <li class="nav-item"><a href="#" class="nav-link "><span class="icon s7-plus"></span><span class="name">Adicionar Novo</span></a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <!--Search input-->
                    <div class="search" style="display: none;">
                        <input type="text" id="search" placeholder="Buscar por clientes ou negócios..." name="q">
                        <span class="s7-search"></span>
                    </div>
                    <div id="list-search" style="display: none;position: absolute;top: 48px;left: 805px;width: 335px;margin-top: 24px;" class="dropdown-menu">
                    </div>
                </div>
            </nav>
            <div class="main-content container">
                <?= $this->Flash->render() ?>
                <?= $this->fetch('content') ?>
            </div>
        </div>
        <div id="md-default" role="dialog" class="modal fade" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" class="close"><span class="s7-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <div class="text-primary"><span class="modal-main-icon s7-headphones"></span></div>
                            <h3 class="mb-4">Canais de Atendimento:</h3>
                            <p>Email: suporte@agence.com.br<br></p>
                            <p>Telefone: +55 (11) 3554-2187<br></p>
                            <div class="mt-6">
                                <button type="button" data-dismiss="modal" class="btn btn-sm btn-space btn-primary">Fechar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
        </footer>
        <?php
        echo $this->Html->script('jquery-3.3.1.min.js');
        echo $this->Html->script('perfect-scrollbar.jquery.min.js');
        echo $this->Html->script('bootstrap.bundle.min.js');
        echo $this->Html->script('bootstrap-slider.min.js');
        echo $this->Html->script('app.js');
        echo $this->Html->script('fuelux/js/wizard.js');
        echo $this->Html->script('jQuery.countdown/dist/jquery.countdown.min.js');
        echo $this->Html->script('select2/dist/js/select2.full.js');
        echo $this->Html->script('select2/dist/js/i18n/pt-BR.js');
        echo $this->Html->script('jQuery-Mask-Plugin/src/jquery.mask.js');
        echo $this->Html->script('jquery-validation/src/core.js');
        echo $this->Html->script('jquery-validation/src/default-settings.js');
        echo $this->Html->script('jquery-validation/src/localization/messages_pt_BR.js');
        echo $this->Html->script('bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js');
        echo $this->Html->script('bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js');
        echo $this->fetch('script');
        echo $this->fetch('scriptChart')
        ?>
        <script type="text/javascript">
            $(document).ready(function () {
                //initialize the javascript
                App.init();
            });
        </script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    </body>
</html>