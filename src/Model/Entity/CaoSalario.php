<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CaoSalario Entity
 *
 * @property string $co_usuario
 * @property \Cake\I18n\FrozenDate $dt_alteracao
 * @property float $brut_salario
 * @property float $liq_salario
 */
class CaoSalario extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'brut_salario' => true,
        'liq_salario' => true
    ];
}
