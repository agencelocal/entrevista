<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CaoCliente Entity
 *
 * @property int $co_cliente
 * @property string|null $no_razao
 * @property string|null $no_fantasia
 * @property string|null $no_contato
 * @property string|null $nu_telefone
 * @property string|null $nu_ramal
 * @property string|null $nu_cnpj
 * @property string|null $ds_endereco
 * @property int|null $nu_numero
 * @property string|null $ds_complemento
 * @property string $no_bairro
 * @property string|null $nu_cep
 * @property string|null $no_pais
 * @property int|null $co_ramo
 * @property int $co_cidade
 * @property int|null $co_status
 * @property string|null $ds_site
 * @property string|null $ds_email
 * @property string|null $ds_cargo_contato
 * @property string|null $tp_cliente
 * @property string|null $ds_referencia
 * @property int|null $co_complemento_status
 * @property string|null $nu_fax
 * @property string|null $ddd2
 * @property string|null $telefone2
 */
class CaoCliente extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'no_razao' => true,
        'no_fantasia' => true,
        'no_contato' => true,
        'nu_telefone' => true,
        'nu_ramal' => true,
        'nu_cnpj' => true,
        'ds_endereco' => true,
        'nu_numero' => true,
        'ds_complemento' => true,
        'no_bairro' => true,
        'nu_cep' => true,
        'no_pais' => true,
        'co_ramo' => true,
        'co_cidade' => true,
        'co_status' => true,
        'ds_site' => true,
        'ds_email' => true,
        'ds_cargo_contato' => true,
        'tp_cliente' => true,
        'ds_referencia' => true,
        'co_complemento_status' => true,
        'nu_fax' => true,
        'ddd2' => true,
        'telefone2' => true,
        'cao_fatura' => true
    ];
}
