<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CaoFatura Entity
 *
 * @property int $co_fatura
 * @property int $co_cliente
 * @property int $co_sistema
 * @property int $co_os
 * @property int $num_nf
 * @property float $total
 * @property float $valor
 * @property \Cake\I18n\FrozenDate $data_emissao
 * @property string $corpo_nf
 * @property float $comissao_cn
 * @property float $total_imp_inc
 */
class CaoFatura extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'co_cliente' => true,
        'co_sistema' => true,
        'co_os' => true,
        'num_nf' => true,
        'total' => true,
        'valor' => true,
        'data_emissao' => true,
        'corpo_nf' => true,
        'comissao_cn' => true,
        'total_imp_inc' => true,
        'cao_cliente' => true,
    ];
}
