<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CaoO Entity
 *
 * @property int $co_os
 * @property int|null $nu_os
 * @property int|null $co_sistema
 * @property string|null $co_usuario
 * @property int|null $co_arquitetura
 * @property string|null $ds_os
 * @property string|null $ds_caracteristica
 * @property string|null $ds_requisito
 * @property \Cake\I18n\FrozenDate|null $dt_inicio
 * @property \Cake\I18n\FrozenDate|null $dt_fim
 * @property int|null $co_status
 * @property string|null $diretoria_sol
 * @property \Cake\I18n\FrozenDate|null $dt_sol
 * @property string|null $nu_tel_sol
 * @property string|null $ddd_tel_sol
 * @property string|null $nu_tel_sol2
 * @property string|null $ddd_tel_sol2
 * @property string|null $usuario_sol
 * @property \Cake\I18n\FrozenDate|null $dt_imp
 * @property \Cake\I18n\FrozenDate|null $dt_garantia
 * @property int|null $co_email
 * @property int|null $co_os_prospect_rel
 */
class CaoO extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nu_os' => true,
        'co_sistema' => true,
        'co_usuario' => true,
        'co_arquitetura' => true,
        'ds_os' => true,
        'ds_caracteristica' => true,
        'ds_requisito' => true,
        'dt_inicio' => true,
        'dt_fim' => true,
        'co_status' => true,
        'diretoria_sol' => true,
        'dt_sol' => true,
        'nu_tel_sol' => true,
        'ddd_tel_sol' => true,
        'nu_tel_sol2' => true,
        'ddd_tel_sol2' => true,
        'usuario_sol' => true,
        'dt_imp' => true,
        'dt_garantia' => true,
        'co_email' => true,
        'co_os_prospect_rel' => true
    ];
}
