<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CaoUsuario Entity
 *
 * @property string $co_usuario
 * @property string $no_usuario
 * @property string $ds_senha
 * @property string|null $co_usuario_autorizacao
 * @property int|null $nu_matricula
 * @property \Cake\I18n\FrozenDate|null $dt_nascimento
 * @property \Cake\I18n\FrozenDate|null $dt_admissao_empresa
 * @property \Cake\I18n\FrozenDate|null $dt_desligamento
 * @property \Cake\I18n\FrozenTime|null $dt_inclusao
 * @property \Cake\I18n\FrozenDate|null $dt_expiracao
 * @property string|null $nu_cpf
 * @property string|null $nu_rg
 * @property string|null $no_orgao_emissor
 * @property string|null $uf_orgao_emissor
 * @property string|null $ds_endereco
 * @property string|null $no_email
 * @property string|null $no_email_pessoal
 * @property string|null $nu_telefone
 * @property \Cake\I18n\FrozenTime $dt_alteracao
 * @property string|null $url_foto
 * @property string|null $instant_messenger
 * @property int|null $icq
 * @property string|null $msn
 * @property string|null $yms
 * @property string|null $ds_comp_end
 * @property string|null $ds_bairro
 * @property string|null $nu_cep
 * @property string|null $no_cidade
 * @property string|null $uf_cidade
 * @property \Cake\I18n\FrozenDate|null $dt_expedicao
 */
class CaoUsuario extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'no_usuario' => true,
        'ds_senha' => true,
        'co_usuario_autorizacao' => true,
        'nu_matricula' => true,
        'dt_nascimento' => true,
        'dt_admissao_empresa' => true,
        'dt_desligamento' => true,
        'dt_inclusao' => true,
        'dt_expiracao' => true,
        'nu_cpf' => true,
        'nu_rg' => true,
        'no_orgao_emissor' => true,
        'uf_orgao_emissor' => true,
        'ds_endereco' => true,
        'no_email' => true,
        'no_email_pessoal' => true,
        'nu_telefone' => true,
        'dt_alteracao' => true,
        'url_foto' => true,
        'instant_messenger' => true,
        'icq' => true,
        'msn' => true,
        'yms' => true,
        'ds_comp_end' => true,
        'ds_bairro' => true,
        'nu_cep' => true,
        'no_cidade' => true,
        'uf_cidade' => true,
        'dt_expedicao' => true
    ];
}
