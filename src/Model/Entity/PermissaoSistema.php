<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PermissaoSistema Entity
 *
 * @property string $co_usuario
 * @property int $co_tipo_usuario
 * @property int $co_sistema
 * @property string $in_ativo
 * @property string|null $co_usuario_atualizacao
 * @property \Cake\I18n\FrozenTime $dt_atualizacao
 */
class PermissaoSistema extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'in_ativo' => true,
        'co_usuario_atualizacao' => true,
        'dt_atualizacao' => true
    ];
}
