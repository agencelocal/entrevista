<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CaoUsuario Model
 *
 * @method \App\Model\Entity\CaoUsuario get($primaryKey, $options = [])
 * @method \App\Model\Entity\CaoUsuario newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CaoUsuario[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CaoUsuario|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CaoUsuario|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CaoUsuario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CaoUsuario[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CaoUsuario findOrCreate($search, callable $callback = null, $options = [])
 */
class CaoUsuarioTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('cao_usuario');
        $this->setDisplayField('no_usuario');
        $this->setPrimaryKey('co_usuario');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->scalar('co_usuario')
                ->maxLength('co_usuario', 20)
                ->allowEmpty('co_usuario', 'create')
                ->add('co_usuario', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
                ->scalar('no_usuario')
                ->maxLength('no_usuario', 50)
                ->requirePresence('no_usuario', 'create')
                ->notEmpty('no_usuario');

        $validator
                ->scalar('ds_senha')
                ->maxLength('ds_senha', 14)
                ->requirePresence('ds_senha', 'create')
                ->notEmpty('ds_senha');

        $validator
                ->scalar('co_usuario_autorizacao')
                ->maxLength('co_usuario_autorizacao', 20)
                ->allowEmpty('co_usuario_autorizacao');

        $validator
                ->allowEmpty('nu_matricula');

        $validator
                ->date('dt_nascimento')
                ->allowEmpty('dt_nascimento');

        $validator
                ->date('dt_admissao_empresa')
                ->allowEmpty('dt_admissao_empresa');

        $validator
                ->date('dt_desligamento')
                ->allowEmpty('dt_desligamento');

        $validator
                ->dateTime('dt_inclusao')
                ->allowEmpty('dt_inclusao');

        $validator
                ->date('dt_expiracao')
                ->allowEmpty('dt_expiracao');

        $validator
                ->scalar('nu_cpf')
                ->maxLength('nu_cpf', 14)
                ->allowEmpty('nu_cpf');

        $validator
                ->scalar('nu_rg')
                ->maxLength('nu_rg', 20)
                ->allowEmpty('nu_rg');

        $validator
                ->scalar('no_orgao_emissor')
                ->maxLength('no_orgao_emissor', 10)
                ->allowEmpty('no_orgao_emissor');

        $validator
                ->scalar('uf_orgao_emissor')
                ->maxLength('uf_orgao_emissor', 2)
                ->allowEmpty('uf_orgao_emissor');

        $validator
                ->scalar('ds_endereco')
                ->maxLength('ds_endereco', 150)
                ->allowEmpty('ds_endereco');

        $validator
                ->scalar('no_email')
                ->maxLength('no_email', 100)
                ->allowEmpty('no_email');

        $validator
                ->scalar('no_email_pessoal')
                ->maxLength('no_email_pessoal', 100)
                ->allowEmpty('no_email_pessoal');

        $validator
                ->scalar('nu_telefone')
                ->maxLength('nu_telefone', 64)
                ->allowEmpty('nu_telefone');

        $validator
                ->dateTime('dt_alteracao')
                ->requirePresence('dt_alteracao', 'create')
                ->notEmpty('dt_alteracao');

        $validator
                ->scalar('url_foto')
                ->maxLength('url_foto', 255)
                ->allowEmpty('url_foto');

        $validator
                ->scalar('instant_messenger')
                ->maxLength('instant_messenger', 80)
                ->allowEmpty('instant_messenger');

        $validator
                ->nonNegativeInteger('icq')
                ->allowEmpty('icq');

        $validator
                ->scalar('msn')
                ->maxLength('msn', 50)
                ->allowEmpty('msn');

        $validator
                ->scalar('yms')
                ->maxLength('yms', 50)
                ->allowEmpty('yms');

        $validator
                ->scalar('ds_comp_end')
                ->maxLength('ds_comp_end', 50)
                ->allowEmpty('ds_comp_end');

        $validator
                ->scalar('ds_bairro')
                ->maxLength('ds_bairro', 30)
                ->allowEmpty('ds_bairro');

        $validator
                ->scalar('nu_cep')
                ->maxLength('nu_cep', 10)
                ->allowEmpty('nu_cep');

        $validator
                ->scalar('no_cidade')
                ->maxLength('no_cidade', 50)
                ->allowEmpty('no_cidade');

        $validator
                ->scalar('uf_cidade')
                ->maxLength('uf_cidade', 2)
                ->allowEmpty('uf_cidade');

        $validator
                ->date('dt_expedicao')
                ->allowEmpty('dt_expedicao');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['co_usuario']));

        return $rules;
    }

}
