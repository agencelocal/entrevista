<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CaoFatura Model
 *
 * @method \App\Model\Entity\CaoFatura get($primaryKey, $options = [])
 * @method \App\Model\Entity\CaoFatura newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CaoFatura[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CaoFatura|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CaoFatura|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CaoFatura patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CaoFatura[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CaoFatura findOrCreate($search, callable $callback = null, $options = [])
 */
class CaoFaturaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cao_fatura');
        $this->setDisplayField('co_fatura');
        $this->setPrimaryKey('co_fatura');
        
        $this->belongsTo('CaoCliente', [
            'foreignKey' => 'co_cliente',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('co_fatura')
            ->allowEmpty('co_fatura', 'create');

        $validator
            ->integer('co_cliente')
            ->requirePresence('co_cliente', 'create')
            ->notEmpty('co_cliente');

        $validator
            ->integer('co_sistema')
            ->requirePresence('co_sistema', 'create')
            ->notEmpty('co_sistema');

        $validator
            ->integer('co_os')
            ->requirePresence('co_os', 'create')
            ->notEmpty('co_os');

        $validator
            ->integer('num_nf')
            ->requirePresence('num_nf', 'create')
            ->notEmpty('num_nf');

        $validator
            ->numeric('total')
            ->requirePresence('total', 'create')
            ->notEmpty('total');

        $validator
            ->numeric('valor')
            ->requirePresence('valor', 'create')
            ->notEmpty('valor');

        $validator
            ->date('data_emissao')
            ->requirePresence('data_emissao', 'create')
            ->notEmpty('data_emissao');

        $validator
            ->scalar('corpo_nf')
            ->requirePresence('corpo_nf', 'create')
            ->notEmpty('corpo_nf');

        $validator
            ->numeric('comissao_cn')
            ->requirePresence('comissao_cn', 'create')
            ->notEmpty('comissao_cn');

        $validator
            ->numeric('total_imp_inc')
            ->requirePresence('total_imp_inc', 'create')
            ->notEmpty('total_imp_inc');

        return $validator;
    }
}
