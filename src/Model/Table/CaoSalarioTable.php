<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CaoSalario Model
 *
 * @method \App\Model\Entity\CaoSalario get($primaryKey, $options = [])
 * @method \App\Model\Entity\CaoSalario newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CaoSalario[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CaoSalario|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CaoSalario|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CaoSalario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CaoSalario[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CaoSalario findOrCreate($search, callable $callback = null, $options = [])
 */
class CaoSalarioTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cao_salario');
        $this->setDisplayField('co_usuario');
        $this->setPrimaryKey(['co_usuario', 'dt_alteracao']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('co_usuario')
            ->maxLength('co_usuario', 20)
            ->allowEmpty('co_usuario', 'create');

        $validator
            ->date('dt_alteracao')
            ->allowEmpty('dt_alteracao', 'create');

        $validator
            ->numeric('brut_salario')
            ->requirePresence('brut_salario', 'create')
            ->notEmpty('brut_salario');

        $validator
            ->numeric('liq_salario')
            ->requirePresence('liq_salario', 'create')
            ->notEmpty('liq_salario');

        return $validator;
    }
}
