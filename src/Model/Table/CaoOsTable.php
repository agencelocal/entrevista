<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CaoOs Model
 *
 * @method \App\Model\Entity\CaoO get($primaryKey, $options = [])
 * @method \App\Model\Entity\CaoO newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CaoO[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CaoO|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CaoO|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CaoO patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CaoO[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CaoO findOrCreate($search, callable $callback = null, $options = [])
 */
class CaoOsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cao_os');
        $this->setDisplayField('co_os');
        $this->setPrimaryKey('co_os');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('co_os')
            ->allowEmpty('co_os', 'create');

        $validator
            ->integer('nu_os')
            ->allowEmpty('nu_os');

        $validator
            ->integer('co_sistema')
            ->allowEmpty('co_sistema');

        $validator
            ->scalar('co_usuario')
            ->maxLength('co_usuario', 50)
            ->allowEmpty('co_usuario');

        $validator
            ->integer('co_arquitetura')
            ->allowEmpty('co_arquitetura');

        $validator
            ->scalar('ds_os')
            ->maxLength('ds_os', 200)
            ->allowEmpty('ds_os');

        $validator
            ->scalar('ds_caracteristica')
            ->maxLength('ds_caracteristica', 200)
            ->allowEmpty('ds_caracteristica');

        $validator
            ->scalar('ds_requisito')
            ->maxLength('ds_requisito', 200)
            ->allowEmpty('ds_requisito');

        $validator
            ->date('dt_inicio')
            ->allowEmpty('dt_inicio');

        $validator
            ->date('dt_fim')
            ->allowEmpty('dt_fim');

        $validator
            ->integer('co_status')
            ->allowEmpty('co_status');

        $validator
            ->scalar('diretoria_sol')
            ->maxLength('diretoria_sol', 50)
            ->allowEmpty('diretoria_sol');

        $validator
            ->date('dt_sol')
            ->allowEmpty('dt_sol');

        $validator
            ->scalar('nu_tel_sol')
            ->maxLength('nu_tel_sol', 20)
            ->allowEmpty('nu_tel_sol');

        $validator
            ->scalar('ddd_tel_sol')
            ->maxLength('ddd_tel_sol', 5)
            ->allowEmpty('ddd_tel_sol');

        $validator
            ->scalar('nu_tel_sol2')
            ->maxLength('nu_tel_sol2', 20)
            ->allowEmpty('nu_tel_sol2');

        $validator
            ->scalar('ddd_tel_sol2')
            ->maxLength('ddd_tel_sol2', 5)
            ->allowEmpty('ddd_tel_sol2');

        $validator
            ->scalar('usuario_sol')
            ->maxLength('usuario_sol', 50)
            ->allowEmpty('usuario_sol');

        $validator
            ->date('dt_imp')
            ->allowEmpty('dt_imp');

        $validator
            ->date('dt_garantia')
            ->allowEmpty('dt_garantia');

        $validator
            ->integer('co_email')
            ->allowEmpty('co_email');

        $validator
            ->integer('co_os_prospect_rel')
            ->allowEmpty('co_os_prospect_rel');

        return $validator;
    }
}
