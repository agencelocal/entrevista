<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CaoCliente Model
 *
 * @method \App\Model\Entity\CaoCliente get($primaryKey, $options = [])
 * @method \App\Model\Entity\CaoCliente newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CaoCliente[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CaoCliente|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CaoCliente|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CaoCliente patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CaoCliente[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CaoCliente findOrCreate($search, callable $callback = null, $options = [])
 */
class CaoClienteTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cao_cliente');
        $this->setDisplayField('no_fantasia');
        $this->setPrimaryKey('co_cliente');
        
        $this->hasMany('CaoFatura', [
            'foreignKey' => 'co_cliente',
            'dependent' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('co_cliente')
            ->allowEmpty('co_cliente', 'create');

        $validator
            ->scalar('no_razao')
            ->maxLength('no_razao', 50)
            ->allowEmpty('no_razao');

        $validator
            ->scalar('no_fantasia')
            ->maxLength('no_fantasia', 50)
            ->allowEmpty('no_fantasia');

        $validator
            ->scalar('no_contato')
            ->maxLength('no_contato', 30)
            ->allowEmpty('no_contato');

        $validator
            ->scalar('nu_telefone')
            ->maxLength('nu_telefone', 15)
            ->allowEmpty('nu_telefone');

        $validator
            ->scalar('nu_ramal')
            ->maxLength('nu_ramal', 6)
            ->allowEmpty('nu_ramal');

        $validator
            ->scalar('nu_cnpj')
            ->maxLength('nu_cnpj', 18)
            ->allowEmpty('nu_cnpj');

        $validator
            ->scalar('ds_endereco')
            ->maxLength('ds_endereco', 150)
            ->allowEmpty('ds_endereco');

        $validator
            ->integer('nu_numero')
            ->allowEmpty('nu_numero');

        $validator
            ->scalar('ds_complemento')
            ->maxLength('ds_complemento', 150)
            ->allowEmpty('ds_complemento');

        $validator
            ->scalar('no_bairro')
            ->maxLength('no_bairro', 50)
            ->requirePresence('no_bairro', 'create')
            ->notEmpty('no_bairro');

        $validator
            ->scalar('nu_cep')
            ->maxLength('nu_cep', 10)
            ->allowEmpty('nu_cep');

        $validator
            ->scalar('no_pais')
            ->maxLength('no_pais', 50)
            ->allowEmpty('no_pais');

        $validator
            ->allowEmpty('co_ramo');

        $validator
            ->requirePresence('co_cidade', 'create')
            ->notEmpty('co_cidade');

        $validator
            ->nonNegativeInteger('co_status')
            ->allowEmpty('co_status');

        $validator
            ->scalar('ds_site')
            ->maxLength('ds_site', 50)
            ->allowEmpty('ds_site');

        $validator
            ->scalar('ds_email')
            ->maxLength('ds_email', 50)
            ->allowEmpty('ds_email');

        $validator
            ->scalar('ds_cargo_contato')
            ->maxLength('ds_cargo_contato', 50)
            ->allowEmpty('ds_cargo_contato');

        $validator
            ->scalar('tp_cliente')
            ->maxLength('tp_cliente', 2)
            ->allowEmpty('tp_cliente');

        $validator
            ->scalar('ds_referencia')
            ->maxLength('ds_referencia', 100)
            ->allowEmpty('ds_referencia');

        $validator
            ->nonNegativeInteger('co_complemento_status')
            ->allowEmpty('co_complemento_status');

        $validator
            ->scalar('nu_fax')
            ->maxLength('nu_fax', 15)
            ->allowEmpty('nu_fax');

        $validator
            ->scalar('ddd2')
            ->maxLength('ddd2', 10)
            ->allowEmpty('ddd2');

        $validator
            ->scalar('telefone2')
            ->maxLength('telefone2', 20)
            ->allowEmpty('telefone2');

        return $validator;
    }
    
    public function beforeFind($event, $query, $options, $primary) {
        $query->order(['CaoCliente.no_fantasia' => 'ASC']);
    }       
}
