<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PermissaoSistema Model
 *
 * @method \App\Model\Entity\PermissaoSistema get($primaryKey, $options = [])
 * @method \App\Model\Entity\PermissaoSistema newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PermissaoSistema[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PermissaoSistema|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PermissaoSistema|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PermissaoSistema patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PermissaoSistema[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PermissaoSistema findOrCreate($search, callable $callback = null, $options = [])
 */
class PermissaoSistemaTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('permissao_sistema');
        //$this->setDisplayField('co_usuario');
        $this->setPrimaryKey(['co_usuario', 'co_tipo_usuario', 'co_sistema']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->scalar('co_usuario')
                ->maxLength('co_usuario', 20)
                ->allowEmpty('co_usuario', 'create');

        $validator
                ->allowEmpty('co_tipo_usuario', 'create');

        $validator
                ->allowEmpty('co_sistema', 'create');

        $validator
                ->scalar('in_ativo')
                ->maxLength('in_ativo', 1)
                ->requirePresence('in_ativo', 'create')
                ->notEmpty('in_ativo');

        $validator
                ->scalar('co_usuario_atualizacao')
                ->maxLength('co_usuario_atualizacao', 20)
                ->allowEmpty('co_usuario_atualizacao');

        $validator
                ->dateTime('dt_atualizacao')
                ->requirePresence('dt_atualizacao', 'create')
                ->notEmpty('dt_atualizacao');

        return $validator;
    }

}
